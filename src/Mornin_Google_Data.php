<?php

/**
 * Class Mornin_Google_Data
 */

class Mornin_Google_Data{

    /**
     * @param $data
     */
    public function getTable($data){
        $table = null;

        if (count($data->getRows()) > 0) {
            $table .= '<table>';

            // Print headers.
            $table .= '<tr>';

            foreach ($data->getColumnHeaders() as $header) {
                $table .= '<th>' . $header->name . '</th>';
            }
            $table .= '</tr>';

            // Print table rows.
            foreach ($data->getRows() as $row) {
                $table .= '<tr>';
                foreach ($row as $cell) {
                    $table .= '<td>'
                        . htmlspecialchars($cell, ENT_NOQUOTES)
                        . '</td>';
                }
                $table .= '</tr>';
            }
            $table .= '</table>';

        } else {
            $table .= '<p>No results found.</p>';
        }

        return $table;

    }

    /**
     * @param $data
     */
    public function getCSV($data){
        $csv = null;
        if (count($data->getRows()) > 0) {
            foreach ($data->getColumnHeaders() as $header) {
                $csv .= '' . $header->name . ',';
            }
            $csv .= "\n";

            // Print table rows.
            foreach ($data->getRows() as $row) {
                foreach ($row as $cell) {
                    $csv .= ''
                        . htmlspecialchars($cell, ENT_NOQUOTES)
                        . ',';
                }
                $csv .= "\n";
            }
        } else {
            $csv .= '<p>No results found.</p>';
        }

        return $csv;
    }
}
