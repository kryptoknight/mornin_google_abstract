<?php

/**
 * Class Mornin_Google_Auth
 */

class Mornin_Google_Auth{

    public $client;

    /**
     * @param $client Google Client Object
     */
    public function __construct(){
        $this->client = new Google_Client();
        $this->client->setApplicationName('Google Report Generator');
        $this->client->setClientId('667432675219-hbu20ebm2m7oelqsrtbmo23onm0visqv.apps.googleusercontent.com');
        $this->client->setClientSecret('Pe_LG_IFWLPh8pBvw3PDYNwT');
        $this->client->setRedirectUri('http://ops.morninglory.com/');
        $this->client->setDeveloperKey('AIzaSyAjAzBWt4xohJ43NOdUtyKZzXC0JyqqD3o');
        $this->client->setScopes(array('https://www.googleapis.com/auth/analytics.readonly'));
        //$this->client->setUseObjects(true);
    }

    public function isLoggedIn(){

    }

    public function logout(){
        unset($_SESSION['token']);
    }

    public function authenticate($code){
        $this->client->authenticate($code);
        $_SESSION['token'] = $this->client->getAccessToken();
       $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
        header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
    }
    public function getClient(){
        return $this->client;
    }
}
